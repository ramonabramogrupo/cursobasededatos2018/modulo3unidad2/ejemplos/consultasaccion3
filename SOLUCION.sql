﻿/*
  CONSULTAS ACCION 3
*/

/*
  EJERCICIO 1
*/

  CREATE DATABASE IF NOT EXISTS consultasAccion3;
  USE consultasAccion3;

/*
  EJERCICIO 2
*/

  -- FICHERO EMPLEADOS.SQL

-- ---------------------------------------------------------------------------------

/*
  EJERCICIO 2
*/

  CREATE TABLE IF NOT EXISTS importes(
    tipo varchar(20),
    valor int,
    PRIMARY KEY (tipo)
    );

  INSERT INTO importes VALUES
    ('Jornalero',25),
    ('Efectivo',250);

/*
  EJERCICIO 3
*/

  -- ALTER TABLE empleados ADD COLUMN Salario_Basico float DEFAULT 0;

  UPDATE empleados 
    JOIN importes ON Tipo_trabajo=tipo 
    SET Salario_Basico=valor*
        IF(Tipo_trabajo='jornalero',Horas_trabajadas,Dias_trabajdos);

  -- NO SERIA CORRECTO PORQUE NO UTILIZA LA TABLA IMPORTES
  UPDATE empleados 
    SET Salario_Basico=
        IF(Tipo_trabajo='jornalero',25*Horas_trabajadas,250*Dias_trabajdos);

  -- FUNCIONA
   UPDATE empleados 
    JOIN importes ON Tipo_trabajo=tipo 
    SET Salario_Basico=(Horas_trabajadas+Dias_trabajdos)*valor;

  /*
    EJERCICIO 4
  */
  
  -- ALTER TABLE empleados ADD COLUMN premios float;

  UPDATE empleados  
    SET premios=Salario_Basico*
      CASE
        WHEN 
          (Rubro='Chofer' OR Rubro='Azafata') 
          AND 
          (Dias_trabajdos>20 OR Horas_trabajadas>200) 
          THEN 0.15
        WHEN 
          (NOT (Rubro='Chofer' OR Rubro='Azafata')) 
          AND 
          (Dias_trabajdos>20 OR Horas_trabajadas>200) 
          THEN 0.05
        ELSE 0
      END;

  UPDATE empleados e  
    SET e.premios=e.Salario_Basico*
      IF(e.Horas_trabajadas>200 OR e.Dias_trabajdos>20,
        IF((Rubro='Chofer' OR Rubro='Azafata'),0.15,0.05),0);

/* ejercicio 5 */

-- ALTER TABLE empleados ADD COLUMN sueldoNominal float;

UPDATE empleados SET sueldoNominal = Salario_Basico + premios;

/* ejercicio 6 */

 -- crear campo nuevo BPS en la tabla EMPLEADOS
  -- ALTER TABLE empleados DROP COLUMN bps;
  -- ALTER TABLE empleados ADD COLUMN bps float;

  -- crear campo nuevo IRP en la tabla EMPLEADOS
  -- ALTER TABLE empleados DROP COLUMN irpf;
  -- ALTER TABLE empleados ADD COLUMN irpf float;

  -- calcular BPS
  UPDATE empleados
    SET bps=sueldoNominal*0.13;

  -- calcular IRPF
   UPDATE empleados
      SET irpf=sueldoNominal*
         CASE 
            WHEN sueldoNominal<4*1160 THEN 0.03
            WHEN sueldoNominal<=10*1160 THEN 0.06
            ELSE 0.09
         END;

  UPDATE empleados e
  SET e.IRPF=e.sueldoNominal* 
    IF
      (e.sueldoNominal<=(4*1160),0.03,
    IF(e.sueldoNominal>=(10*1160),0.09
        ,0.06));

  /* ejercicio 7 */
  -- ALTER TABLE empleados DROP COLUMN SubtotalDescuentos;
  -- ALTER TABLE empleados ADD COLUMN SubtotalDescuentos float;

  UPDATE empleados
    SET SubtotalDescuentos=BPS+IRPF;



  /* ejercicio 8  */

-- ALTER TABLE empleados ADD COLUMN IF NOT EXISTS sueldoliquido float;

UPDATE empleados e
  SET sueldoliquido=(e.sueldoNominal-e.subtotaldescuentos);
  
  
  /* ejercicio 9 */
  
-- ALTER TABLE empleados DROP COLUMN IF EXISTS vTransporte;
-- ALTER TABLE empleados ADD COLUMN IF NOT EXISTS vTransporte float;

UPDATE empleados e
  SET e.vTransporte = 
  CASE
  WHEN 
    (e.Barrio = 'Cordon' OR e.Barrio = 'Centro') 
    AND 
    (DATEDIFF(NOW(),e.`Fecha-nac`)/365 > 40) 
    THEN 150
  WHEN 
    NOT(e.Barrio = 'Cordon' OR e.Barrio = 'Centro') 
    AND 
    (DATEDIFF(NOW(),e.`Fecha-nac`)/365 > 40) 
    THEN 200
  ELSE 100
  END;

/* ejercicio 10 */
  
  -- ALTER TABLE empleados ADD COLUMN VALIMENTACION float DEFAULT 0;

  UPDATE empleados 
    SET VALIMENTACION= 
      CASE 
        WHEN sueldoliquido<5000 THEN 300
        WHEN sueldoliquido<=10000 THEN  
          IF(Tipo_trabajo='jornalero',200,100)
        ELSE 0
      END;

                                       









